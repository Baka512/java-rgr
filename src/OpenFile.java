import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * Придназначен для работы с диалоговым окном 
 * класса JFileChooser при открытии файла
 * @author Илья Могилин
 */
public class OpenFile {
    private JFileChooser fc;      
    public OpenFile(){
        fc = new JFileChooser();
        fc.setAcceptAllFileFilterUsed(false);
        fc.setMultiSelectionEnabled(false);
        fc.setFileFilter(new FileNameExtensionFilter("текстовый документ .txt", "txt"));
        fc.setCurrentDirectory(new File("H:\\"));
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    }
    /** @return 0, если файл выбран */
    public int Open(){
        return fc.showDialog(null, "Открыть файл");
    }
    /** @return Путь к файлу */
    public String getAbsolutePath(){
        return fc.getSelectedFile().getAbsolutePath();
    }    
    /** @return Имя файла */
    public String getName(){
        return fc.getSelectedFile().getName();
    }   
}
