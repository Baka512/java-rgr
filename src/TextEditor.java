import java.io.*;
import javax.swing.*;
/**
 * Предназначен для работы с файлом на диске
 * @author Илья Могилин
 */
public class TextEditor {
    private String FileAbsolutePath, FileName;
    private boolean isNew;
    /**
     * Конструктор для нового, пустого, несохранённого файла
     */
    public TextEditor(){
        FileAbsolutePath = FileName = "Безымянный.txt";
        isNew = true;
    }
    /**
     * Конструктор для открытия файла<br/>
     * @param param - объект класса OpenFile
     */
    public TextEditor(OpenFile param){      
        FileAbsolutePath = param.getAbsolutePath(); 
        FileName = param.getName();
        isNew = false;    
    }
    /**
     * Конструктор для сохранения копии текущей конфигурации файла<br/>
     * и последующей с ней работы
     * @param param - объект класса SaveFile
     */
    public TextEditor(SaveFile param){
        FileAbsolutePath = param.getAbsolutePath(); 
        FileName = param.getName();           
        if (!FileAbsolutePath.substring(FileAbsolutePath.length()-4, FileAbsolutePath.length()).equals(".txt"))
                    FileAbsolutePath += ".txt";        
        isNew = false;
    }
    /**
     * isNew - true,  если файл новый<br/>
     * isNew - false, если файл ренее сохранялся на диск
     * @return boolean isNew
     */
    public boolean isNew(){
        return isNew;
    }
    /**
     * Сравнивает параметр ST с его предыдущим значением
     * @param ST - String
     * @return boolean
     */
    public boolean isChanged(String ST){
        if (isNew)
           return !ST.equals("");
        else
           return !ST.equals(Read());
    }
    /**
     * Возвращает имя файла
     * @return String FileName
     */
    public String getFileName(){
        return FileName;
    }
    /**
     * Считывайет содержимое файла и возвращает через String
     * @return String
     */    
    public String Read(){      
	try {
            BufferedReader in = new BufferedReader(new InputStreamReader( new FileInputStream(FileAbsolutePath)));
            int i = 0;
            String Line, S = "";
            while ((Line = in.readLine()) != null){
                if (i>0) 
                    S += "\n" + Line;
                else
                    S += Line;
                i++;
            }
            in.close();
            return S;
	}
	catch(IOException ex) {
            return null;
	}
    }
    /**
     * Сохраняет файл
     * @param St String - сохраняемое содержимое файла
     */
    public void Save(String St){          
        try (FileWriter myFile = new FileWriter(FileAbsolutePath)) {
            myFile.write(St);
         } catch (IOException ex) {
        }
    }
    /**
     * Диалог, предупреждающий пользователя о действие, 
     * которое может повлечь потерю несохранённых данных
     * @return 0 (Сохранить), 1 (Не сохранять), 2 (Отмена)
     */
    public int SaveDialog(){
        Object[] options = {"Сохранить", "Не сохранять", "Отмена"};
            return JOptionPane.showOptionDialog(
                null,
                "Вы хотите сохранить изменения в файле\n" + FileName + "?", 
                "Editor",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[2]
            );
    }
}