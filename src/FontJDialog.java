import java.awt.Font;
import java.awt.GraphicsEnvironment;
/**
 * @author Илья Могилин
 */
public class FontJDialog extends javax.swing.JDialog {
	// Элементы формы
    private javax.swing.JButton   Button1;
    private javax.swing.JButton   Button2;
    private javax.swing.JComboBox ComboBox1;
    private javax.swing.JComboBox ComboBox2;
    private javax.swing.JComboBox ComboBox3;
    private javax.swing.JLabel 	  Label1;
    private javax.swing.JLabel    Label2;
	// Параметры формы
    private Font ChangedFont, font;
    
    public FontJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        // Инициализация компонент формы
        ComboBox1 = new javax.swing.JComboBox();
        ComboBox2 = new javax.swing.JComboBox();
        ComboBox3 = new javax.swing.JComboBox();
        Button1   = new javax.swing.JButton();
        Button2   = new javax.swing.JButton();
        Label1    = new javax.swing.JLabel();
        Label2    = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        GraphicsEnvironment GE = GraphicsEnvironment.getLocalGraphicsEnvironment();
        String[] Items = GE.getAvailableFontFamilyNames();
        ComboBox1.setModel(new javax.swing.DefaultComboBoxModel(Items));
        ComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        Items = new String[] {
                "обычный",
                "полужирный",
                "курсив",
                "полужирный курсив"
        };
        ComboBox2.setModel(new javax.swing.DefaultComboBoxModel(Items));
        ComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });
        
        Items = new String[12];
        int j = 0;
        for (int i = 8; i<=30; i+=2)
            Items[j++] = Integer.toString(i);
        ComboBox3.setModel(new javax.swing.DefaultComboBoxModel(Items));
        ComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        Button1.setText("ОК");
        Button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        Button2.setText("Отмена");
        Button2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        Label1.setText("Пример текста");

        Label2.setText("Text example");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Button1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Button2)
                .addGap(20, 20, 20))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Label2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(ComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(Label1, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 8, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(Label1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Label2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Button2)
                    .addComponent(Button1))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        pack();
        
        // Выставление стандартных значений для jComboBox-ов      
        ChangedFont = parent.getFont();
        ComboBox1.setSelectedItem(ChangedFont.getFontName());
        ComboBox2.setSelectedIndex(ChangedFont.getStyle());
        ComboBox3.setSelectedIndex((ChangedFont.getSize()-8)/2);
    }
    // нажатие на Ок
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ChangedFont = font;
        setVisible(false);
        dispose();
    }
    // нажатие на Отмена
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        setVisible(false);
        dispose();
    }
    // Изменение jComboBox1
    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {
        String FontName = ComboBox1.getSelectedItem().toString();
        int Style = ComboBox2.getSelectedIndex();
        int Size =  ComboBox3.getSelectedIndex()*2+8;
        font = new Font(FontName,Style,Size);
        Label1.setFont(font);
        Label2.setFont(font);
    }
    // Изменение jComboBox2
    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {
        String FontName = ComboBox1.getSelectedItem().toString();
        int Style = ComboBox2.getSelectedIndex();
        int Size =  ComboBox3.getSelectedIndex()*2+8;
        font = new Font(FontName,Style,Size);
        Label1.setFont(font);
        Label2.setFont(font);
    }
    // Изменение jComboBox3
    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {
        String FontName = ComboBox1.getSelectedItem().toString();
        int Style = ComboBox2.getSelectedIndex();
        int Size =  ComboBox3.getSelectedIndex()*2+8;
        font = new Font(FontName,Style,Size);
        Label1.setFont(font);
        Label2.setFont(font);
    }
    /** @return возвращает выбранный пользователем Шрифт */
    public Font getNewFont(){
        setVisible(true);
        return   ChangedFont;
    }
}
