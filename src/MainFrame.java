import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
/**
 * Класс формы главного окна
 * @author Илья Могилин
 */
public class MainFrame extends javax.swing.JFrame {
	private javax.swing.JMenuBar 			 MenuBar;
	private javax.swing.JMenu     			 FileMenu;
    private javax.swing.JMenuItem 			 FileOpt1;
    private javax.swing.JMenuItem 			 FileOpt2;
    private javax.swing.JMenuItem 			 FileOpt3;
    private javax.swing.JMenuItem 			 FileOpt4;
    private javax.swing.JPopupMenu.Separator Separator;
    private javax.swing.JMenuItem 			 Exit;
    private javax.swing.JMenu     			 FormatMenu;
    private javax.swing.JMenuItem 			 FormatOpt1;
    private javax.swing.JCheckBoxMenuItem 	 FormatOpt2;
    private javax.swing.JScrollPane 		 ScrollPane;
    private javax.swing.JTextArea 			 textArea;
    // 
    private Font font;
    private TextEditor Editor;
    private boolean WarpStyle;
    //
    public MainFrame() {
        ScrollPane = new javax.swing.JScrollPane();
        textArea   = new javax.swing.JTextArea();
        MenuBar    = new javax.swing.JMenuBar();
        FileMenu   = new javax.swing.JMenu();
        FileOpt1   = new javax.swing.JMenuItem();
        FileOpt2   = new javax.swing.JMenuItem();
        FileOpt3   = new javax.swing.JMenuItem();
        FileOpt4   = new javax.swing.JMenuItem();
        Separator  = new javax.swing.JPopupMenu.Separator();
        Exit       = new javax.swing.JMenuItem();
        FormatMenu = new javax.swing.JMenu();
        FormatOpt1 = new javax.swing.JMenuItem();
        FormatOpt2 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconImage(new ImageIcon("text-editor.png").getImage());
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        textArea.setColumns(20);
        textArea.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        textArea.setRows(5);
        ScrollPane.setViewportView(textArea);

        FileMenu.setText("Файл");

        FileOpt1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        FileOpt1.setText("создать");
        FileOpt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileOpt1ActionPerformed(evt);
            }
        });
        FileMenu.add(FileOpt1);

        FileOpt2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        FileOpt2.setText("открыть");
        FileOpt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileOpt2ActionPerformed(evt);
            }
        });
        FileMenu.add(FileOpt2);

        FileOpt3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        FileOpt3.setText("сохранить");
        FileOpt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileOpt3ActionPerformed(evt);
            }
        });
        FileMenu.add(FileOpt3);

        FileOpt4.setText("сохранить как...");
        FileOpt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FileOpt4ActionPerformed(evt);
            }
        });
        FileMenu.add(FileOpt4);
        FileMenu.add(Separator);

        Exit.setText("Выход");
        Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitActionPerformed(evt);
            }
        });
        FileMenu.add(Exit);

        MenuBar.add(FileMenu);

        FormatMenu.setText("Формат");

        FormatOpt1.setText("Шрифт...");
        FormatOpt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FormatOpt1ActionPerformed(evt);
            }
        });
        FormatMenu.add(FormatOpt1);

        FormatOpt2.setText("Перенос по словам");
        FormatOpt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FormatOpt2ActionPerformed(evt);
            }
        });
        FormatMenu.add(FormatOpt2);

        MenuBar.add(FormatMenu);

        setJMenuBar(MenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 884, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
        );
        pack();
        
        textArea.setText("");
        Editor = new TextEditor();
        this.setTitle(Editor.getFileName());
        WarpStyle = false;
    }
    // Создание
    private void FileOpt1ActionPerformed(java.awt.event.ActionEvent evt) {
        if (Editor.isChanged(textArea.getText())){
            switch(Editor.SaveDialog()){
                case 0: if (Editor.isNew()){
                            SaveFile S = new SaveFile();
                            if (S.Save() == 0){
                                Editor = new TextEditor(S);
                                Editor.Save(textArea.getText());
                            }
                            else
                                break;
                        }
                        else
                            Editor.Save(textArea.getText());
                case 1: textArea.setText("");
                        Editor = new TextEditor();
                        this.setTitle(Editor.getFileName());
            }
            return;
        }
        textArea.setText("");
        Editor = new TextEditor();
        this.setTitle(Editor.getFileName());

    }
    // Открытие
    private void FileOpt2ActionPerformed(java.awt.event.ActionEvent evt) {
        if (Editor.isChanged(textArea.getText())){
            switch(Editor.SaveDialog()){
                case 0: if (Editor.isNew()){
                        SaveFile S = new SaveFile();
                        if (S.Save() == 0){
                            Editor = new TextEditor(S);
                            Editor.Save(textArea.getText());
                        }
                        else
                            break;
                    }
                    else
                        Editor.Save(textArea.getText());
                case 1: OpenFile O = new OpenFile();
                        if (O.Open() == 0){
                            Editor = new TextEditor(O);
                            textArea.setText(Editor.Read());
                            this.setTitle(Editor.getFileName());
                            textArea.setCaretPosition(0);
                        }
            }
            return;
        }
        OpenFile O = new OpenFile();
        if (O.Open() == 0){
            Editor = new TextEditor(O);
            textArea.setText(Editor.Read());
            this.setTitle(Editor.getFileName());
            textArea.setCaretPosition(0);
        }
    }
    // Сохранение
    private void FileOpt3ActionPerformed(java.awt.event.ActionEvent evt) {
        if (Editor.isNew()){
            SaveFile S = new SaveFile();
            if (S.Save() == 0){
                Editor = new TextEditor(S);
                Editor.Save(textArea.getText());
            }
        }
        else
            Editor.Save(textArea.getText());
    }
    // Сохранить как...
    private void FileOpt4ActionPerformed(java.awt.event.ActionEvent evt) {
        SaveFile S = new SaveFile();
        if (S.Save() == 0){
            Editor = new TextEditor(S);
            Editor.Save(textArea.getText());
        }
    }
    // Выход
    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {
        if (Editor.isChanged(textArea.getText())){
            switch(Editor.SaveDialog()){
                case 0: if (Editor.isNew()){
                        SaveFile S = new SaveFile();
                        if (S.Save() == 0){
                            Editor = new TextEditor(S);
                            Editor.Save(textArea.getText());
                        }
                        else
                            break;
                    }
                    else
                        Editor.Save(textArea.getText());
                case 1: dispose();
            }
            return;
        }
        dispose();
    }
    // Формат
    private void FormatOpt1ActionPerformed(java.awt.event.ActionEvent evt) {
        font = textArea.getFont();
        FontJDialog F = new FontJDialog(this, true);
        font = F.getNewFont();
        textArea.setFont(font);
        setVisible(true);
    }
    // Перенос строк
    private void FormatOpt2ActionPerformed(java.awt.event.ActionEvent evt) {
        WarpStyle = !WarpStyle;
        textArea.setLineWrap(WarpStyle);
    }    
    // Выход по "крестиу"
    private void formWindowClosing(java.awt.event.WindowEvent evt) {
        if (Editor.isChanged(textArea.getText())){
            switch(Editor.SaveDialog()){
                case 0: if (Editor.isNew()){
                        SaveFile S = new SaveFile();
                        if (S.Save() == 0){
                            Editor = new TextEditor(S);
                            Editor.Save(textArea.getText());
                        }
                        else
                            break;
                    }
                    else
                        Editor.Save(textArea.getText());
                case 1: dispose();
            }
            return;
        }
        dispose();
    }
    /** @return Возвращает текущий Шрифт */
    public Font getFont(){
        return font;
    }
    // Запуск формы
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                }
                catch (Exception e) { }
                new MainFrame().setVisible(true);
            }
        });
    }
}
