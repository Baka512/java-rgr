import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * Придназначен для работы с диалоговым окном 
 * класса JFileChooser при сохранении файла
 * @author Илья Могилин
 */
public class SaveFile {
    private JFileChooser fc;
    public SaveFile(){
        fc = new JFileChooser();
        fc.setAcceptAllFileFilterUsed(false);
        fc.setMultiSelectionEnabled(false);
        fc.setFileFilter(new FileNameExtensionFilter("текстовый документ .txt", "txt"));
        fc.setCurrentDirectory(new File("H:\\"));
    }
    /** @return 0, если файл выбран */
    public int Save(){
        return fc.showSaveDialog(null);
    }
    /** @return Путь к файлу */
    public String getAbsolutePath(){
        return fc.getSelectedFile().getAbsolutePath();
    }   
    /** @return Имя файла */
    public String getName(){
        return fc.getSelectedFile().getName();
    }   
}
